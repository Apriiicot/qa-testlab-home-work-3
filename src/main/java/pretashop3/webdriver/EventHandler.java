package pretashop3.webdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventHandler implements WebDriverEventListener {

    private static final Logger log = LoggerFactory.getLogger(EventHandler.class);

    public void beforeAlertAccept(WebDriver webDriver) {

    }

    public void afterAlertAccept(WebDriver webDriver) {

    }

    public void afterAlertDismiss(WebDriver webDriver) {

    }

    public void beforeAlertDismiss(WebDriver webDriver) {

    }

    public void beforeNavigateTo(String s, WebDriver webDriver) {
        log.info("----------------------------------------------");
        log.info("Navigate to URL: ", s);
        log.info("Current URL is: {}", webDriver.getCurrentUrl());
        log.info("Title: {}", webDriver.getTitle());
    }

    public void afterNavigateTo(String s, WebDriver webDriver) {
        log.info("----------------------------------------------");
        log.info("Current URL is: {}", webDriver.getCurrentUrl());
        log.info("Title: {}", webDriver.getTitle());
    }

    public void beforeNavigateBack(WebDriver webDriver) {

    }

    public void afterNavigateBack(WebDriver webDriver) {

    }

    public void beforeNavigateForward(WebDriver webDriver) {
    }

    public void afterNavigateForward(WebDriver webDriver) {

    }

    public void beforeNavigateRefresh(WebDriver webDriver) {

    }

    public void afterNavigateRefresh(WebDriver webDriver) {

    }

    public void beforeFindBy(By by, WebElement webElement, WebDriver webDriver) {
        log.info("----------------------------------------------");
        log.info("Before find element: {}", by.toString());
        log.info("Current URL is: {}", webDriver.getCurrentUrl());
        log.info("Title: {}", webDriver.getTitle());
    }

    public void afterFindBy(By by, WebElement webElement, WebDriver webDriver) {
        log.info("----------------------------------------------");
        log.info("After find element: {}", by.toString());
        log.info("Current URL is: {}", webDriver.getCurrentUrl());
        log.info("Title: {}", webDriver.getTitle());
    }

    public void beforeClickOn(WebElement webElement, WebDriver webDriver) {
        log.info("----------------------------------------------");
        log.info("Before click on element: {}", webElement.getText());
        log.info("Element is displayed: {}", webElement.isDisplayed());
        log.info("Element is enable: {}", webElement.isEnabled());
        log.info("Current URL is: {}", webDriver.getCurrentUrl());
        log.info("Title: {}", webDriver.getTitle());
    }

    public void afterClickOn(WebElement webElement, WebDriver webDriver) {
        log.info("----------------------------------------------");
        log.info("Current URL is: {}", webDriver.getCurrentUrl());
        log.info("Title: {}", webDriver.getTitle());
    }

    public void beforeChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {

    }

    public void afterChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {

    }

    public void beforeScript(String s, WebDriver webDriver) {

    }

    public void afterScript(String s, WebDriver webDriver) {

    }

    public void onException(Throwable throwable, WebDriver webDriver) {

    }
}
