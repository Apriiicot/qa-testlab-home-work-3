package pretashop3.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.BrowserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pretashop3.model.Category;
import pretashop3.model.User;
import pretashop3.pages.LoginPage;

public class CreateCategoryTest extends BaseScript {

    public static void main(String[] args) throws InterruptedException {
        User user = User.createValidUser();
        Category category = Category.createValidCategory();

        WebDriver driver = getConfiguredDriver(BrowserType.FIREFOX);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.loginUser(user)
                          .getSideBarElement()
                          .moveToCategory()
                          .addNewCategory()
                          .saveNewCategory(category)
                          .fitterCategoriesByName(category);
    }
}
