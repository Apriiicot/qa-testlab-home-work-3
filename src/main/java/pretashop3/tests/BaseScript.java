package pretashop3.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import pretashop3.webdriver.EventHandler;

import java.io.File;
import java.util.concurrent.TimeUnit;

public abstract class BaseScript {

    public static WebDriver getDriver(String browser) {
        if(browser.equals(BrowserType.CHROME)){
            System.setProperty(
                    "webdriver.chrome.driver",
                    new File(BaseScript.class.getResource("/chromedriver.exe").getFile()).getPath());
            return new ChromeDriver();
        }else if(browser.equals(BrowserType.FIREFOX)){
            System.setProperty(
                    "webdriver.gecko.driver",
                    new File(BaseScript.class.getResource("/geckodriver.exe").getFile()).getPath());
            return new FirefoxDriver();
        }else {
            System.setProperty(
                    "webdriver.chrome.driver",
                    new File(BaseScript.class.getResource("/chromedriver.exe").getFile()).getPath());
            return new ChromeDriver();
        }
    }

    public static EventFiringWebDriver getConfiguredDriver(String browser) {
        EventFiringWebDriver driver = new EventFiringWebDriver(getDriver(browser));
        driver.manage().timeouts()
                       .implicitlyWait(10, TimeUnit.SECONDS)
                       .pageLoadTimeout(15, TimeUnit.SECONDS) ;
        driver.register(new EventHandler());
        return driver;
    }
}
