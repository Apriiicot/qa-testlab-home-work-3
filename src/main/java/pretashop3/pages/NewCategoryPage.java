package pretashop3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pretashop3.model.Category;

public class NewCategoryPage extends AbstractPageObject {

    public NewCategoryPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "name_1")
    private WebElement categoryName;

    @FindBy(id = "category_form_submit_btn")
    private WebElement saveCategoryButton;

    public CategoriesPage saveNewCategory(Category category){
        waitForContentLoad(By.id("name_1"));
        categoryName.sendKeys(category.getName());
        saveCategoryButton.click();
        return new CategoriesPage(driver);
    }
}
