package pretashop3.pages;

import org.openqa.selenium.WebDriver;

public class HomePage extends AbstractPageObject {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public SideBarElement getSideBarElement(){
        return new SideBarElement(driver);
    }
}
