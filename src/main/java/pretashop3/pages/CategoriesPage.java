package pretashop3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pretashop3.model.Category;

public class CategoriesPage extends AbstractPageObject {

    public CategoriesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "desc-category-new")
    private WebElement newCategoryButton;

    @FindBy(name = "categoryFilter_name")
    private WebElement categoryNameFilter;

    @FindBy(id = "submitFilterButtoncategory")
    private WebElement submitFilter;

    public NewCategoryPage addNewCategory(){
        waitForContentLoad(By.id("desc-category-new"));
        newCategoryButton.click();
        return new NewCategoryPage(driver);
    }

    public CategoriesPage fitterCategoriesByName(Category category){
        waitForContentLoad(By.name("categoryFilter_name"));
        categoryNameFilter.sendKeys(category.getName());
        submitFilter.click();
        waitForContentLoad(By.xpath(String.format("//td[contains(text(),'%s')]", category.getName())));
        return this;
    }
}
