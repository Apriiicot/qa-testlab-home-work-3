package pretashop3.model;

public class User {

    public User() {
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    private String email;
    private String password;

    public static User createValidUser(){
        return new User("webinar.test@gmail.com", "Xcg7299bnSmMuRLp9ITw");
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
