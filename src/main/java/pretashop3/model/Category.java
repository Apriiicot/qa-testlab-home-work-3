package pretashop3.model;

public class Category {

    private String name;

    public Category(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Category createValidCategory(){
        return new Category("Test-Category");
    }
}
